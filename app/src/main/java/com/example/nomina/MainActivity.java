package com.example.nomina;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtnombre;
    private Button btnEntrar;
    private Button btnSalir;
    private Operaciones operaciones;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtnombre = (EditText) findViewById(R.id.txtNombre);
        btnEntrar = (Button)findViewById(R.id.btnEntrar);
        btnSalir = (Button)findViewById(R.id.btnSalir);
        operaciones = new Operaciones(txtnombre.getText().toString());

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cliente = txtnombre.getText().toString();
                if(cliente.matches(""))
                    Toast.makeText(MainActivity.this,"Ingrese un nombre",Toast.LENGTH_SHORT).show();
                else{
                    operaciones.setNombre(cliente);
                    Bundle objeto = new Bundle();
                    objeto.putSerializable("operaciones",operaciones);
                    Intent intent = new Intent(MainActivity.this, NominaPago.class);
                    intent.putExtras(objeto);
                    startActivity(intent);
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
