package com.example.nomina;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class NominaPago extends AppCompatActivity {

    private TextView lblNombre2;
    private TextView lblRecibo;
    private EditText HorasT;
    private EditText HorasE;
    private TextView lblSubTotal;
    private TextView lblImpuestos;
    private TextView lblTotalP;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private Operaciones operaciones;
    private RadioGroup rgOpciones;
    private double Resul;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nomina_pago);

        lblNombre2 = (TextView) findViewById(R.id.lblNombre2);
        lblRecibo = (TextView) findViewById(R.id.lblRecibo);
        HorasT = (EditText)findViewById(R.id.txtHorasT);
        HorasE = (EditText)findViewById(R.id.txtHorasE);
        lblSubTotal = (TextView)findViewById(R.id.lblSubTotal);
        lblImpuestos = (TextView)findViewById(R.id.lblImpuestos);
        lblTotalP = (TextView)findViewById(R.id.lblTotalP);
        btnCalcular = (Button)findViewById(R.id.btnCalcular);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnRegresar = (Button)findViewById(R.id.btnRegresar);
        rgOpciones = (RadioGroup)findViewById(R.id.rgOpciones);

        Bundle datos = getIntent().getExtras();
        operaciones = (Operaciones)datos.getSerializable("operaciones");

        lblNombre2.setText(String.valueOf(operaciones.getNombre()));
        lblRecibo.setText(String.valueOf(operaciones.generaFolio()));

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rgOpciones.getCheckedRadioButtonId() == R.id.rdb1){
                    String veri = HorasT.getText().toString();
                    String veri2 = HorasE.getText().toString();
                    if (veri.matches(""))
                    {
                        Toast.makeText(NominaPago.this,"Ingrese horas de Trabajo",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        if (veri2.matches(""))
                        {
                            Toast.makeText(NominaPago.this,"Ingrese horas Extras",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Resul=(Double.valueOf(HorasT.getText().toString()).doubleValue())*50;
                            Resul=((Double.valueOf(HorasE.getText().toString()).doubleValue())*100)+Resul;
                            lblSubTotal.setText(String.valueOf(Resul));
                            Double Resul2=Resul*.16;
                            lblImpuestos.setText(String.valueOf(Resul2));
                            Resul=Resul-Resul2;
                            lblTotalP.setText(String.valueOf(Resul));
                        }
                    }

                }
                else if (rgOpciones.getCheckedRadioButtonId() == R.id.rdb2){
                    String veri = HorasT.getText().toString();
                    String veri2 = HorasE.getText().toString();
                    if (veri.matches(""))
                    {
                        Toast.makeText(NominaPago.this,"Ingrese horas de Trabajo",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        if (veri2.matches(""))
                        {
                            Toast.makeText(NominaPago.this,"Ingrese horas Extras",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Resul=(Double.valueOf(HorasT.getText().toString()).doubleValue())*70;
                            Resul=((Double.valueOf(HorasE.getText().toString()).doubleValue())*140)+Resul;
                            lblSubTotal.setText(String.valueOf(Resul));
                            Double Resul2=Resul*.16;
                            lblImpuestos.setText(String.valueOf(Resul2));
                            Resul=Resul-Resul2;
                            lblTotalP.setText(String.valueOf(Resul));
                        }
                    }
                }
                else {
                    Resul=(Double.valueOf(HorasT.getText().toString()).doubleValue())*100;
                    Resul=((Double.valueOf(HorasE.getText().toString()).doubleValue())*200)+Resul;
                    lblSubTotal.setText(String.valueOf(Resul));
                    Double Resul2=Resul*.16;
                    lblImpuestos.setText(String.valueOf(Resul2));
                    Resul=Resul-Resul2;
                    lblTotalP.setText(String.valueOf(Resul));
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblImpuestos.setText("");
                lblSubTotal.setText("");
                lblTotalP.setText("");
                HorasE.setText("");
                HorasT.setText("");
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}
