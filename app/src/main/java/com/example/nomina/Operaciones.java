package com.example.nomina;

import java.io.Serializable;
import java.util.Random;

public class Operaciones implements Serializable {

    private String nombre;

    public Operaciones(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int generaFolio(){
        return new Random().nextInt(1000)%1000;
    }
}
